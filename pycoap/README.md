
To install:
    git clone https://ericwilkison@bitbucket.org/ericwilkison/pycoap.git

    cd pycoap
    pip3 install .

    or

    pip3 install -e . (to install by symlinks)


NOTE: Testing requires asynctest
   pip3 install asynctest