import asyncio
import logging
import subprocess
import sys
import unittest

import asynctest
from asynctest import mock

import pycoap.constants as C
from pycoap.client import Client
from pycoap.exceptions import NameResolutionError, MessageTimeoutError, ProtocolError


class Test(asynctest.TestCase):
    

    @classmethod
    def setUpClass(cls):
        print("\nStarting COAP server")
        cls.coapSerrver = subprocess.Popen(["coap-server"] )

    @classmethod
    def tearDownClass(cls):
        print("\nStopping COAP server")
        cls.coapSerrver.kill()


    async def test_bad_host_name(self):
        uri = "coap://host-name-does-not-exist.no/test"
        client = Client()
        with self.assertRaises(NameResolutionError):
            await client.get(uri)  

    @unittest.skipUnless('--slow' in sys.argv, "Not runnig slow tests")
    def test_request_timed_out(self):
        uri = "coap://4.2.2.2/test"
        client = Client()
        with self.assertRaises(MessageTimeoutError):
            self.loop.run_until_complete( client.get(uri) )   


    async def test_request_connection_refused(self):
        uri = "coap://127.0.0.1:2222/"
        client = Client()
        with self.assertRaises(ProtocolError):
            await client.get(uri)    


    def test_root(self):
        uri = "coap://localhost/"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertTrue( str(result.payload).startswith("This is a test server made with libcoap") )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")



    @unittest.skipUnless('--slow' in sys.argv, "Not runnig slow tests")
    def test_observe_timed_out(self):
        async def test():
            uri = "coap://4.2.2.2/test"
            client = Client()
            obs = await client.observe(uri)   

            with self.assertRaises(MessageTimeoutError):
                await obs.register()
                    # async for res in obs.observe():
                    #     pass

        self.loop.run_until_complete( test() )



    def test_observe_connection_refused(self):
        async def test():
            uri = "coap://127.0.0.1:2222/"
            client = Client()
            obs = await client.observe(uri)   
            await obs.register()
            with self.assertRaises(ProtocolError):
                async for res in obs.observe(): #exception raised here
                    pass  
        
        self.loop.run_until_complete( test() )


    @unittest.skipUnless('--slow' in sys.argv, "Not runnig slow tests")
    def test_observe_time(self):
        async def test():
            uri = "coap://127.0.0.1/time"
            client = Client()
            count = 0
            lastObsNum = -1
            obs = await client.observe(uri)   
            await obs.register()
            async for result in obs.observe():
                self.assertEqual(str(result.code), "2.05")
                self.assertGreater(int(result.options["Observe"]),lastObsNum)                
                if count >= 4:
                    await obs.deregister()
                    break
                count +=1
                lastObsNum = int(result.options["Observe"]) 
        self.loop.run_until_complete( test() )



if __name__ == '__main__':
   
    unittest.main()
