import asyncio
import time
import unittest

import asynctest
from asynctest import mock

import pycoap.constants as C
from pycoap.exceptions import InvalidRequest
from pycoap.observe import Observe
from pycoap.packet.packet import Packet

OBSERVE_SET = [Packet("ACK", C.CODE_CONTENT, messageID=x, payload="The Data", token="A", observe=x) for x in range(10,20) ]

OBSERVE_OUT_OF_ORDER_SET = [
    Packet("ACK", C.CODE_CONTENT, messageID=12, payload="The Data 12", token="A", observe=12),
    Packet("ACK", C.CODE_CONTENT, messageID=11, payload="The Data 11", token="A", observe=11),
    Packet("ACK", C.CODE_CONTENT, messageID=13, payload="The Data 13", token="A", observe=13),
]

class Test(asynctest.TestCase):

    async def test_create(self):
        """Test create """
        obs = await Observe.create("1.1.1.1",1111, "CON", C.METHOD_GET, )
        self.assertIsInstance(obs, Observe)
        self.assertEqual(obs.destIP,"1.1.1.1" )
        self.assertEqual(obs.port, 1111)
        self.assertEqual(obs.messageType,"CON")
        self.assertEqual(str(obs.code), "0.01")
        self.assertEqual(len(obs.token), 4)
        obs.stop()

    async def test_init_with_invalid_message_type(self):
        with self.assertRaises(InvalidRequest):
            obs = await Observe.create("1.1.1.1",1112,"ACK",C.METHOD_GET)
            obs.stop()
        with self.assertRaises(InvalidRequest):
            obs = await Observe.create("1.1.1.1",1113,"RST",C.METHOD_GET)
            obs.stop()

    async def test_init_with_invalid_code(self):
        with self.assertRaises(InvalidRequest):
            obs = await Observe.create("1.1.1.1",1114,"CON",C.CODE_CREATED)
            obs.stop()
        with self.assertRaises(InvalidRequest):
            obs = await Observe.create("1.1.1.1",1115,"CON",C.CODE_BAD_REQUEST)
            obs.stop()

    @mock.patch('pycoap.xmitter.Xmitter.send')
    async def test_register(self, mock_next_layer_send):
        obs = await Observe.create("1.1.1.1",1116, "CON", C.METHOD_GET, )
        await obs.register() 
        obs.stop()

        mock_next_layer_send.assert_called_once()

        sent_packet =  mock_next_layer_send.call_args[0][0] 
        self.assertTrue(sent_packet.hasObserve)
        self.assertEqual(sent_packet.observe, C.OBSERVE_REGISTER)

    @mock.patch('pycoap.xmitter.Xmitter.send')
    async def test_deregister(self, mock_next_layer_send):
        obs = await Observe.create("1.1.1.1",1117, "CON", C.METHOD_GET, )
        await obs.deregister() 
        obs.stop()

        mock_next_layer_send.assert_called_once()

        sent_packet =  mock_next_layer_send.call_args[0][0] 
        self.assertTrue(sent_packet.hasObserve)
        self.assertEqual(sent_packet.observe, C.OBSERVE_DEREGISTER)


    @mock.patch('pycoap.xmitter.Xmitter.receive', side_effect=OBSERVE_SET)
    async def test_observe(self, mock_next_layer_receive):
        """ 
        Test observation 
        
        Test that at least 3 responses are received
        """
        obs = await Observe.create("1.1.1.1",1117, "CON", C.METHOD_GET, token="A" )

        count = 0
        async for response in obs.observe():
            if count > 3:
                break
            self.assertEqual(response.payload, "The Data")
            count += 1

        obs.stop()
 

    @mock.patch('pycoap.xmitter.Xmitter.receive', side_effect=OBSERVE_OUT_OF_ORDER_SET )
    async def test_observe_duplicates(self, mock_next_layer_receive):
        """ 
        Test observation when duplicate packets are returned from source 
        
        OBSERVE_OUT_OF_ORDER_SET induces responses with observe numbers of 
        "The Data 12", "The Data 11", and "The Data 13"
        Test confirms that 11 is not received.
        """
        obs = await Observe.create("1.1.1.1",1117, "CON", C.METHOD_GET, token="A" )
        count = 0
        async for response in obs.observe():
            count += 1
            if count == 1:  self.assertEqual(response.payload, "The Data 12" )
            if count == 2:  self.assertEqual(response.payload, "The Data 13" )

            if count == 2:
                break

        obs.stop()


if __name__ == '__main__':
    unittest.main()
