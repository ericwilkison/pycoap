import asyncio
import logging
import unittest

import asynctest
from asynctest import mock

import pycoap.constants as C
from pycoap.packet.code import Code
from pycoap.exceptions import TokenMismatchError
from pycoap.packet.packet import Packet
from pycoap.request import Request, Response


#logging.basicConfig(level=logging.DEBUG)

@asynctest.fail_on(active_handles=True)
class Test(asynctest.TestCase):

    @mock.patch('pycoap.xmitter.Xmitter.send')
    async def test_create(self, mock_next_layer_send):
        """ Make sure create returns a Request object """
        req = await Request.create("1.1.1.1", 5555, "CON", C.METHOD_GET)
        self.assertIsInstance(req, Request)
        self.assertEqual(req.destIP,"1.1.1.1" )
        self.assertEqual(req.port, 5555 )
        self.assertEqual(req.messageType,"CON")
        self.assertEqual(str(req.code), "0.01")
        self.assertEqual(len(req.token), 4)
        req.stop() #Manually call complete to clean up socket
    
    @mock.patch('pycoap.xmitter.Xmitter.send')
    async def test_send(self, mock_next_layer_send):
        """
        Tests that a packet is sent with accurate values
        """
        req = await Request.create("1.1.1.1", 5555, "CON", C.METHOD_GET, token='A1',
                                   hostname="test-host", pathSegments=["level1","level2"], queryParams=["a=1","b=2"],
                                   contentFormat=C.FORMAT_TEXT, acceptFormat=C.FORMAT_TEXT, payload="The Data")
        await req.send()
        req.stop() #Manually call complete to clean up socket
        
        packet = mock_next_layer_send.call_args[0][0]
        self.assertEqual(packet.messageType, "CON")
        self.assertEqual(packet.code, "0.01")
        self.assertEqual(packet.token, 'A1')
        self.assertEqual(packet.hostname, "test-host")
        self.assertEqual(packet.path, "/level1/level2")
        self.assertEqual(packet.query, "a=1&b=2" )
        self.assertEqual(packet.contentFormat, "text/plain charset=utf-8" )
        self.assertEqual(packet.acceptFormat, "text/plain charset=utf-8" )
        self.assertEqual(packet.payload, "The Data")

    @mock.patch('pycoap.xmitter.Xmitter.receive', return_value=Packet("ACK",
                                                                      C.CODE_CONTENT, 
                                                                      messageID=1, 
                                                                      payload="The Data",
                                                                      token="A"))
    async def test_receive(self, mock_next_layer_receive):
        """ Tests that receive() receives a packet and returns a Response object """

        req = await Request.create("1.1.1.1", 5555, "CON", C.METHOD_GET, token="A")
        response = await req.receive()
        req.stop() #Manually call complete to clean up socket

        self.assertIsInstance(response,Response)
        self.assertEqual(response.code, "2.05")
        self.assertEqual(response.payload, "The Data")

    @mock.patch('pycoap.xmitter.Xmitter.receive', side_effect=RuntimeError("Simulated Error"))
    async def test_receive_errror(self, mock_next_layer_receive):
        """ Tests that receive() raises an error """
        req = await Request.create("1.1.1.1", 5555, "CON", C.METHOD_GET)

        with self.assertRaises(RuntimeError):
            await req.receive()
        
        req.stop() #Manually call complete to clean up socket


    @mock.patch('pycoap.xmitter.Xmitter.receive', return_value=Packet("ACK",C.CODE_CREATED, token="B"))
    async def test_receive_token_mismatch(self, mock_next_layer_receive):
        """ Tests that receive raises exception when responce token does not match """

        req = await Request.create("1.1.1.1", 5555, "CON", C.METHOD_GET, token="A")

        with self.assertRaises(TokenMismatchError):
            await req.receive()

        req.stop() #Manually call complete to clean up socket



