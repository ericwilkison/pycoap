

import unittest

import pycoap.constants as C
from pycoap.packet.packet import Packet
from pycoap.packet.token import Token


class TestPacket(unittest.TestCase):

    def test_eq(self):
        """ Tests __eq__ method """
        p1 = Packet("CON", C.METHOD_GET, messageID=1, token=b'\xFF\xFF')
        p2 = Packet("CON", C.METHOD_GET, messageID=1, token=b'\xFF\xFF')
        self.assertEqual(p1,p2)

    def test_not_eq(self):
        """ Tests __eq__ method """
        p1 = Packet("CON", C.METHOD_GET, messageID=1, token=b'\xFF\x00')
        p2 = Packet("CON", C.METHOD_GET, messageID=1, token=b'\xFF\xFF')
        print(p1)
        self.assertNotEqual(p1,p2)

    def test_copy(self):
        p1 = Packet("CON",C.METHOD_GET, 
                    token=Token(length=4), 
                    pathSegments=["level1","level2"], 
                    queryParams=["a=1","b=2"],
                    payload="The Data")
        p2 = p1.copy()
        self.assertEqual(p1,p2)

        #Prove deep copy by changing attr on p1 and see fi p2 changes
        p2._messageType = "ACK"
        self.assertEqual(p1._messageType, "CON")
        self.assertEqual(p2._messageType, "ACK")
if __name__ == '__main__':
    unittest.main()
