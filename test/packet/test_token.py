

import unittest

import pycoap.constants as C
from pycoap.packet.token import Token

class Test(unittest.TestCase):
    def test_init(self):
        self.assertEqual(Token(), b'')

    def test_init_length(self):
        self.assertEqual(len(Token(length=4)), 4)
        self.assertEqual(len(Token(length=0)), 0)
        self.assertEqual(len(Token(length=8)), 8)

    def test_init_length_invalid(self):
        with self.assertRaises(AssertionError):
           Token(length=-1)
        with self.assertRaises(AssertionError):
           Token(length=9)

    def test_init_bytes(self):
        Token(b'')
        Token(b'\xAF\xAF')
        Token(b'\x01\x02\x03\x04\x05\x06\x07\x08')

    def test_init_bytes_invalid(self):
        with self.assertRaises(AssertionError):
           Token(b'\x01\x02\x03\x04\x05\x06\x07\x08\x09') #Longer then 8 bytes

    def test_init_str(self):
        Token("")
        Token("TEST")
        Token("12345678")

    def test_init_str_invalid(self):
        with self.assertRaises(AssertionError):
           Token(b'123456789') #Longer then 8 bytes


if __name__ == '__main__':
    unittest.main()

