

import unittest

import pycoap.constants as C
from pycoap.packet.options import (Block1Option, Block2Option, HostOption,
                                   ObserveOption, Option, OptionList,
                                   PathOption, PortOption, QueryOption)


class TestOptionsList(unittest.TestCase):

    def test_copy(self):
        o1 = OptionList()
        o1.append( HostOption("host") )
        o1.append( PortOption( 1111 ) )
        o1.append( PathOption( "level1") )
        o1.append( PathOption( "level2") )
        o1.append( QueryOption( "a=1") )
        o1.append( QueryOption( "b=2") )

        o2 = o1.copy()
        self.assertEqual( o2.getHostname(), "host" )
        self.assertEqual( o2.getPort(), 1111 )
        self.assertEqual( o2.getPath(), "/level1/level2" )
        self.assertEqual( o2.getQuery(), "a=1&b=2")


class TestObserve(unittest.TestCase):
    def test_init(self):
        self.assertEqual(ObserveOption(1), 1)

class TestBlock1(unittest.TestCase):
    def test_init(self):
        b = Block1Option( (2**20, True, 1024) )
        b = Block1Option( (3, True, 1024) )
        b = Block1Option( (0, True, 16) )
        b = Block1Option( (0, True, 32) )
        b = Block1Option( (0, True, 64) )
        b = Block1Option( (0, True, 128) )
        b = Block1Option( (0, True, 256) )
        b = Block1Option( (0, True, 512) )
        b = Block1Option( (0, True, 1024) )
        b = Block1Option( (0, False, 16) )

        b = Block1Option( (2, False, 16) )
        self.assertEqual(b.blockNum, 2)
        self.assertEqual(b.moreBlocks, False)
        self.assertEqual(b.blockSize, 16)

    def test_init_invalid(self):
        with self.assertRaises(AssertionError):
            Block1Option( (-1, True, 16) )
        with self.assertRaises(AssertionError):
            Block1Option( (2**20+1, True, 1024) )
        with self.assertRaises(AssertionError):
            Block1Option( (0, True, 15) )
        with self.assertRaises(AssertionError):
            Block1Option( (0, True, 8) )
        with self.assertRaises(AssertionError):
            Block1Option( (0, True, 1025) )
        with self.assertRaises(AssertionError):
            Block1Option( (0, True, 2048) )


class TestBlock2(unittest.TestCase):
    def test_init(self):
        b = Block2Option( (2**20, True, 1024) )
        b = Block2Option( (3, True, 1024) )
        b = Block2Option( (0, True, 16) )
        b = Block2Option( (0, True, 32) )
        b = Block2Option( (0, True, 64) )
        b = Block2Option( (0, True, 128) )
        b = Block2Option( (0, True, 256) )
        b = Block2Option( (0, True, 512) )
        b = Block2Option( (0, True, 1024) )
        b = Block2Option( (0, False, 16) )

        b = Block2Option( (2, False, 16) )
        self.assertEqual(b.blockNum, 2)
        self.assertEqual(b.moreBlocks, False)
        self.assertEqual(b.blockSize, 16)

    def test_init_invalid(self):
        with self.assertRaises(AssertionError):
            Block2Option( (-1, True, 16) )
        with self.assertRaises(AssertionError):
            Block2Option( (2**20+1, True, 1024) )
        with self.assertRaises(AssertionError):
            Block2Option( (0, True, 15) )
        with self.assertRaises(AssertionError):
            Block2Option( (0, True, 8) )
        with self.assertRaises(AssertionError):
            Block2Option( (0, True, 1025) )
        with self.assertRaises(AssertionError):
            Block2Option( (0, True, 2048) )



if __name__ == '__main__':
    unittest.main()
