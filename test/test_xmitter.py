import asyncio
import logging
import unittest

import asynctest
from asynctest import mock

import pycoap.constants as C
from pycoap.exceptions import (MessageTimeoutError, NameResolutionError,
                               ProtocolError, ReceiveTimeoutError)
from pycoap.xmitter import Proto, Xmitter
from pycoap.packet.packet import Packet

def _run(coro):
    return asyncio.get_event_loop().run_until_complete(coro)

#logging.basicConfig(level=logging.DEBUG)

@asynctest.fail_on(active_handles=True)
class Test(asynctest.TestCase):
    
    def test_init_bad_ip_1(self):
        with self.assertRaises(ValueError):
            Xmitter("not a hostname", 5555)

    def test_init_bad_ip_2(self):
        with self.assertRaises(ValueError):
            Xmitter("1.1.1.257", 5555)

    def test_init_bad_ip_3(self):
        with self.assertRaises(AssertionError):
            Xmitter( 0x01010101, 5555)

    def test_init_bad_port_1(self):
        with self.assertRaises(AssertionError):
            Xmitter("1.1.1.1", "5555")

    def test_init_bad_port_out_of_range(self):
        with self.assertRaises(AssertionError):
            Xmitter("1.1.1.1", -1)
        with self.assertRaises(AssertionError):
            Xmitter("1.1.1.1", 65536)

    async def test_create2(self):
        x = await Xmitter.create( "1.1.1.1", 5555 )
        self.assertIsNotNone(x.proto)
        self.assertIsNotNone(x.sock)
        x.stop()


    async def test_send_con_timeout_error(self):
        """
        Patches Proto.send to suppress actually sending the packet.
        Patches ACK_TIMEOUT_MIN to speed up testing
        Note: Nothing responds to the request packets
        Tests that MessageTimeoutError is raised by receive()
        Tests that mock_send called multiple times for initial send and retries.
        Tests that each call to mock_send was was made with the request_packet bytes
        """
        request_packet = Packet("CON", C.METHOD_GET, messageID=1)

        patch_send = mock.patch('pycoap.xmitter.Proto.send' )
        patch_ack_timeout = mock.patch('pycoap.constants.ACK_TIMEOUT_MIN', .001) 

        with patch_send as mock_send, patch_ack_timeout:
            x = await Xmitter.create( "1.1.1.1", 5555 )

            with self.assertRaises(MessageTimeoutError):
                await x.send(request_packet)

            self.assertEqual(mock_send.call_count, C.MAX_RETRANSMITS + 1) #Plus 1 for initial send
            for call_args in mock_send.call_args_list:
                self.assertEqual(call_args[0][0], request_packet.toBytes())


            x.stop()


    async def test_send_con_receives_ack(self):
        """
        Sends a request, mock simulates receiving a response ack 

        Tests that retry count is zero,  because ack was received after first send

        Patch replaces the send method of the Proto object.  New method injects a 
        response packet
        """
    
        def replacement_Proto_send(self, data):
            response_packet = Packet("ACK", C.CODE_CONTENT, messageID=1)
            self.datagram_received( response_packet.toBytes(), ( "1.1.1.1", 5555 ) )

        patch_send = mock.patch('pycoap.xmitter.Proto.send', new=replacement_Proto_send ) 

        with patch_send:
            x = await Xmitter.create( "1.1.1.1", 5555 )

            #Send a request
            request_packet = Packet("CON", C.METHOD_GET, messageID=1)
            retry_count = await x.send(request_packet)

            self.assertEqual(retry_count, 0)

            x.stop()

    async def test_send_others(self):
        """
        Patches Proto.send to suppress actually sending the packet.
        Tests that message was sent exactly once, that the retransmit process was not used
        Sub tests are run for message types other than CON
        """
        messageID = 100
        for messageType in ["NON","ACK","RST"]:
            messageID += 1
            with self.subTest(messageType=messageType):

                request_packet = Packet(messageType, C.METHOD_GET, messageID=messageID)

                patch_send = mock.patch('pycoap.xmitter.Proto.send' )

                with patch_send as mock_send:
                    x = await Xmitter.create( "1.1.1.1", 5555 )

                    await x.send(request_packet)

                    self.assertEqual(mock_send.call_count, 1) 

                    x.stop()



    async def test_receive(self):
        """
        Simulates receiving a response packet
        Tests that receive returns the response packet
        """
        ack_packet = Packet("ACK", C.CODE_CONTENT, messageID=1, payload="The Data")

        x = await Xmitter.create( "1.1.1.1", 5555 )

        #Simulate incoming packet
        x.proto.datagram_received( bytes(ack_packet), ( "1.1.1.1", 5555) )

        received_packet = await x.receive()

        self.assertEqual(received_packet, ack_packet)
        x.stop()


    async def test_receive_seperate(self):
        """
        Sends a request, mock simulates receiving an ack packet and a separate data packet.
        Note that only the response_data packet is returned to xmitter.receive because ack
        packet is empty. Tests that ONLY data packet is received by receive()        
        """
        ack = Packet("ACK", C.CODE_EMPTY, messageID=1)
        data = Packet("NON", C.CODE_CONTENT, messageID=2)

        x = await Xmitter.create( "1.1.1.1", 5555 )

        #Simulate incoming packets
        x.proto.datagram_received( bytes(ack), ( "1.1.1.1", 5555) )
        x.proto.datagram_received( bytes(data), ( "1.1.1.1", 5555) )

        received_packet = await x.receive()

        self.assertEqual(received_packet, data)
        x.stop()


    async def test_receive_reset(self):
        """
        Simulates an incoming reset packet
        Test that response raises ProtocolError
        """
        rst_packet = Packet("RST", C.CODE_EMPTY, messageID=1 )

        x = await Xmitter.create( "1.1.1.1", 5555 )

        #Simulate incoming packet
        x.proto.datagram_received( rst_packet.toBytes(), ( "1.1.1.1", 5555) )

        with self.assertRaises(ProtocolError):
            await x.receive()
        x.stop()


    async def test_receive_con(self):
        """
        simulates an incoming CON packet 
        Test that an empty ack packet is sent back
        """
        con_packet = Packet("CON", C.CODE_CREATED, messageID=1)
        
        patch_send = mock.patch('pycoap.xmitter.Proto.send' )
        
        with patch_send as mock_send:
            x = await Xmitter.create( "1.1.1.1", 5555 )

            #Simulate incoming packet
            x.proto.datagram_received( con_packet.toBytes(), ( "1.1.1.1", 5555) )

            mock_send.assert_called_once()

            ack_packet = Packet.from_bytes( mock_send.call_args[0][0] )            
            self.assertEqual(ack_packet.messageType, "ACK")
            self.assertEqual(ack_packet.code, C.CODE_EMPTY)
            self.assertEqual(ack_packet.messageID, 1)

            x.stop()


    async def test_receive_drop_bad_source(self):
        """
        Tests that an incoming packets various attributes are dropped
        
        """
        #Note that each addr is different from the Xmitter.create addr
        addrs = [ ( "2.2.2.2", 1111), ( "1.1.1.1", 2222),  ]
        packet = Packet("ACK", C.CODE_CHANGED, messageID=1 )


        for addr in addrs:
            with self.subTest(addr = addr):
                x = await Xmitter.create( "1.1.1.1", 1111 )

                #Simulate incoming packet, with source address values
                x.proto.datagram_received( packet.toBytes(), addr )

                #Receive queue would return the packet if it was not dropped 
                with self.assertRaises(ReceiveTimeoutError):
                    #Very low timeout to speed up testing
                    await x.receive(timeout=.001)
                x.stop()
                
    async def test_receive_drop_bad_packets(self):
        """
        Tests that an incoming invalid packets are dropped
        """
        data_list = [ Packet("CON", C.CODE_GET, messageID=1 ), #Request packets are dropped
                      Packet("CON", C.CODE_EMPTY, messageID=3 ), #Empty packets are dropped
                      b'junk data' ] # not a parsable coap packet

        for data in data_list:
            with self.subTest(data = data):
                x = await Xmitter.create( "1.1.1.1", 1111 )

                #Simulate incoming packet, with source address values
                x.proto.datagram_received( bytes(data), ("1.1.1.1", 1111) )

                #Receive queue would return the packet if it was not dropped 
                with self.assertRaises(ReceiveTimeoutError):
                    #Very low timeout to speed up testing
                    await x.receive(timeout=.001)
                x.stop()

    async def test_receive_duplicate(self):
        """ Test that duplicate packets are dropped """

        x = await Xmitter.create( "1.1.1.1", 1111 )

        #Simulate incoming packets with duplicate by calling twice
        packet = Packet("ACK",C.CODE_CONTENT, messageID=1000, payload="The Data")
        x.proto.datagram_received(bytes(packet), ( "1.1.1.1", 1111 )  )
        x.proto.datagram_received(bytes(packet), ( "1.1.1.1", 1111 )  )


        first = await x.receive()
        self.assertEqual(first, packet)

        #Receive queue would return the packet if it was not dropped 
        with self.assertRaises(ReceiveTimeoutError):
            #Very low timeout to speed up testing
            await x.receive(timeout=.001)
        x.stop()

    async def test_is_duplicate(self):
        """ Test that _is_duplicate_message returns true the second time it's run """

        x = await Xmitter.create( "1.1.1.1", 1111 )
        self.assertFalse( x._is_duplicate_message(100) )
        self.assertTrue( x._is_duplicate_message(100) )

    async def test_is_duplicate_timeout(self):
        """ 
        Test that _is_duplicate_message returns true the second time it's run 
        Mock is used to change timeout and run test quicker
        """

        with mock.patch("pycoap.constants.MSG_EXCHANGE_LIFETIME", .001):

            x = await Xmitter.create( "1.1.1.1", 1111 )
            self.assertFalse( x._is_duplicate_message(100) )
            await asyncio.sleep(.01)
            self.assertFalse( x._is_duplicate_message(100) )

    async def test_receive_error(self):
        """
        Tests that protocol errors received by Proto and received and thrown
        """
        x = await Xmitter.create( "1.1.1.1", 1111 )

        #Simulate incoming error by manually calling 
        x.proto.error_received( RuntimeError("Simulated Error") )

        #Receive queue would return the packet if it was not dropped 
        with self.assertRaises(ProtocolError):
            await x.receive()
        x.stop()


    async def test_send_conn_refused_error(self):
        """
        Tests that connection error causes await to stop.

        Problem:  When sending a packet that causes a connection refused error, the 
        error would be put on the receive queue, but the send never received and ack.
        This ended up eventually causing a MessageTimeOut Error.  Test makes fure the
        correct error is received
        """

        # Causes an actual Connection Refused Message to be generated
        # because localhost does not have port 1111 open. x.send() does not receive
        # the raisd error.  The error is sent to the receive queue and seen by x.receive
        x = await Xmitter.create( "127.0.0.1", 1111 )
        await x.send(Packet("CON", C.METHOD_GET, messageID=2222))

        with self.assertRaises(ProtocolError):
            await x.receive()
        x.stop()


if __name__ == '__main__':
    unittest.main()
