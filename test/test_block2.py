import asyncio
import logging
import unittest

import asynctest
from asynctest import mock

import pycoap.constants as C
from pycoap.exceptions import TokenMismatchError
from pycoap.packet.packet import Packet
from pycoap.block1 import Block1 as NextLayer
from pycoap.block2 import Block2, Block2PacketOrderError, Block2PayloadSizeError


NEXT_LAYER_SEND = 'pycoap.block1.Block1.send'
NEXT_LAYER_RECEIVE = 'pycoap.block1.Block1.receive'

@asynctest.fail_on(active_handles=True)
class Test(asynctest.TestCase):

    async def test_create(self):
        """
        Tests that Create returns a block1 object
        """
        b = await Block2.create("1.1.1.1",5555)
        self.assertIsInstance(b, Block2)
        self.assertIsInstance(b.nextLayer, NextLayer)

    ###########################################################################
    # Send Tests
    ###########################################################################

    @mock.patch(NEXT_LAYER_SEND)
    async def test_send(self, next_layer_mock):
        b = await Block2.create("1.1.1.1",5555)
        p = Packet("CON",C.METHOD_GET)
        await b.send(p)

        next_layer_mock.assert_called_once_with(p)


    ###########################################################################
    # Receive Tests
    ###########################################################################

    @mock.patch(NEXT_LAYER_RECEIVE, return_value=Packet("ACK", C.CODE_CONTENT, payload="The Data"))
    async def test_receive(self, mock_next_layer_receive):
        """ Tests that Block2 pases non block2 packets throuth with out change"""
        b = await Block2.create("1.1.1.1",5555)
        p = await b.receive()

        self.assertEqual(p.messageType, "ACK")
        self.assertEqual(p.code, C.CODE_CONTENT)
        self.assertEqual(p.payload, "The Data")


    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, return_value=Packet("ACK", C.CODE_CONTENT, token="A", block2=((0,True,16)), payload="A"*16 ) )
    async def test_receive_token_mismatch(self, mock_send, mock_receive):
        """Tests that and invalid token raises an error

            Note that send packet has token 'B' where receive side_effect sends token 'A'
        """
        b = await Block2.create("1.1.1.1",5555)
        await b.send(Packet("CON",C.METHOD_GET, token="B"))
        with self.assertRaises(TokenMismatchError):
            await b.receive()


    #TODO - 
    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, side_effect= [ Packet("ACK", C.CODE_CONTENT, payload="A"*1024, block2=(1,True,1024)),
                                                    Packet("ACK", C.CODE_CONTENT, payload="A"*1024), ])
    async def test_receive_non_block2_after_establish(self, mock_send, mock_receive):
        """ 
        Tests that an exceptin is raised  if a non block2 packet is received after establishing a block2 series
        Note: second packet in mock receive series does not contain a block2 option
        """
        b = await Block2.create("1.1.1.1",5555)
        await b.send(Packet("CON",C.METHOD_GET))
        with self.assertRaises(Block2PacketOrderError):
            await b.receive()


    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, return_value=Packet("ACK", C.CODE_CONTENT, payload="A"*1024, block2=(2,True,1024)))
    async def test_receive_block_order_error(self, mock_send, mock_receive):
        """ Test that exception is raised when the wrong block number is received
            Note: received packet has a block number set to 2.  Block2 expects block 0 as first packet
        """
        b = await Block2.create("1.1.1.1",5555)
        await b.send(Packet("CON",C.METHOD_GET))
        with self.assertRaises(Block2PacketOrderError):
            await b.receive()

    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, return_value=Packet("ACK", C.CODE_CONTENT, payload="A"*32, block2=(0,True,1024)))
    async def test_receive_payload_size_error(self, mock_send, mock_receive):
        """Tests that the received packet payload size matches the block size
            Note: mock receive packet has a pyload size of 32 but a block2 size of 1024
        """
        b = await Block2.create("1.1.1.1",5555)
        await b.send(Packet("CON",C.METHOD_GET))
        with self.assertRaises(Block2PayloadSizeError):
            await b.receive()



    @mock.patch(NEXT_LAYER_RECEIVE, side_effect= [ Packet("ACK", C.CODE_CONTENT, block2=(0,True,1024), payload="A"*1024 ),
                                                   Packet("ACK", C.CODE_CONTENT, block2=(1,True,1024), payload="B"*1024 ),
                                                   Packet("ACK", C.CODE_CONTENT, block2=(2,False,1024), payload="C"*512 ), ])
    @mock.patch(NEXT_LAYER_SEND)
    async def test_receive_complete(self, mock_send, mock_receive):
        """ Simulates a complete block2 series and responce """
        b = await Block2.create("1.1.1.1",5555)
        await b.send(Packet("CON",C.METHOD_GET))
        p = await b.receive()

        self.assertEqual(mock_send.call_count, 3)
        self.assertFalse(mock_send.call_args_list[0][0][0].hasBlock2) #Make sure that first send does not have block2
        self.assertEqual(mock_send.call_args_list[1][0][0].block2Num, 1) #Make sure that second send ask for block number 1
        self.assertEqual(mock_send.call_args_list[2][0][0].block2Num, 2) #Make sure that second send ask for block number 2

        self.assertEqual(mock_receive.call_count, 3)
        self.assertEqual(p.payload, "A"*1024 + "B"*1024 + "C"*512)



if __name__ == '__main__':
    unittest.main()
