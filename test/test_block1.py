import asyncio
import logging
import unittest

import asynctest
from asynctest import mock

import pycoap.constants as C
from pycoap.block1 import (Block1, Block1NotSupportedError,
                           Block1PacketOrderError,
                           Block1UnexpectedResponceError)
from pycoap.exceptions import TokenMismatchError
from pycoap.packet.packet import Packet
from pycoap.xmitter import Xmitter as NextLayer

NEXT_LAYER_SEND = 'pycoap.xmitter.Xmitter.send'
NEXT_LAYER_RECEIVE = 'pycoap.xmitter.Xmitter.receive'
LARGE_DATA = "A"*1024 + "B"*1024 + "C"*512

@asynctest.fail_on(active_handles=True)
class Test(asynctest.TestCase):

    async def test_create(self):
        """
        Tests that Create returns a block1 object
        """
        b = await Block1.create("1.1.1.1",5555)
        self.assertIsInstance(b, Block1)
        self.assertIsInstance(b.nextLayer, NextLayer)

    ###########################################################################
    # Send Tests
    ###########################################################################

    @mock.patch(NEXT_LAYER_SEND)
    async def test_send(self, next_layer_mock):
        b = await Block1.create("1.1.1.1",5555)
        p = Packet("CON",C.METHOD_GET)
        await b.send(p)

        next_layer_mock.assert_called_once_with(p)

    @mock.patch(NEXT_LAYER_SEND)
    async def test_send_large_data(self, next_layer_mock):
        """
        Tests that send detects the large payload, changes the  payload
        to a singel block size (1024) by default, and block one option 
        with the correct values.
        """
        b = await Block1.create("1.1.1.1",5555)
        p = Packet("CON",C.METHOD_GET, payload=LARGE_DATA)
        await b.send(p)
        sentPacket = next_layer_mock.call_args[0][0]

        next_layer_mock.assert_called_once
        self.assertEqual(sentPacket.payload, "A"*1024)
        self.assertTrue(sentPacket.hasBlock1)
        self.assertEqual(sentPacket.block1, (0,True,1024) )  

    ###########################################################################
    # Receive Tests
    ###########################################################################

    @mock.patch(NEXT_LAYER_RECEIVE, return_value=Packet("ACK", C.CODE_CONTENT, payload="The Data"))
    async def test_receive(self, mock_next_layer_receive):
        b = await Block1.create("1.1.1.1",5555)
        p = await b.receive()

        self.assertEqual(p.messageType, "ACK")
        self.assertEqual(p.code, C.CODE_CONTENT)
        self.assertEqual(p.payload, "The Data")



    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, side_effect= [ 
            Packet("ACK", C.CODE_CONTINUE, token="A", block1=(0,True,1024) ),
            Packet("ACK", C.CODE_CONTINUE, token="A", block1=(1,True,1024) ),
            Packet("ACK", C.CODE_CREATED, token="A", block1=(2,False,1024) ), ])
    async def test_receive_continue(self, mock_send, mock_receive):
        """Tests that when receiving a CODE_CONTINUE it sends the next block"""
        b = await Block1.create("1.1.1.1",5555)
        #Send with large data
        await b.send(Packet("CON",C.METHOD_PUT, payload=LARGE_DATA, token="A"))
        response = await b.receive()

        self.assertEqual(mock_send.call_count, 3)
        self.assertEqual(mock_receive.call_count, 3)
        self.assertEqual(response.code, C.CODE_CREATED)


    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, side_effect= [ 
            Packet("ACK", C.CODE_CONTINUE, token="A", block1=(0,True,1024) ),
            Packet("ACK", C.CODE_CONTINUE, token="A", block1=(1,True,1024) ),
            Packet("ACK", C.CODE_CREATED, token="A", block1=(2,False,1024) ), ])
    async def test_receive_token_mismatch(self, mock_send, mock_receive):
        """Tests that and invalid token raises an error

            Note that send packet has token 'B' where receive side_effect sends token 'A'
        """
        b = await Block1.create("1.1.1.1",5555)
        #Send with large data
        await b.send(Packet("CON",C.METHOD_PUT, payload=LARGE_DATA, token="B"))
        with self.assertRaises(TokenMismatchError):
            await b.receive()

    

    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, side_effect= [ 
            Packet("ACK", C.CODE_CONTINUE, token="A", block1=(0,True,1024) ),
            Packet("ACK", C.CODE_CONTINUE, token="A", block1=(2,True,1024) ),
            Packet("ACK", C.CODE_CREATED, token="A", block1=(1,False,1024) ), ])
    async def test_receive_packet_order(self, mock_send, mock_receive):
        """Tests that server packet order errror is raised
            Note that receive side_effect block number order is 0,2,1
        """
        b = await Block1.create("1.1.1.1",5555)
        #Send with large data
        await b.send(Packet("CON",C.METHOD_PUT, payload=LARGE_DATA, token="A"))
        with self.assertRaises(Block1PacketOrderError):
            await b.receive()



    @mock.patch(NEXT_LAYER_SEND)
    @mock.patch(NEXT_LAYER_RECEIVE, return_value=Packet("ACK", C.CODE_CREATED, token="A" ) )
    async def test_receive_not_supported(self, mock_send, mock_receive):
        """Tests that error is raied when the server sends a responce that is missing the block1 option

            Note that mock receive does not send back a block1 option
        """

        b = await Block1.create("1.1.1.1",5555)
        #Send with large data
        await b.send(Packet("CON",C.METHOD_PUT, payload=LARGE_DATA, token="A"))
        with self.assertRaises(Block1NotSupportedError):
            await b.receive()


if __name__ == '__main__':
    unittest.main()
