import asyncio
import logging
import subprocess
import sys
import unittest

import pycoap.constants as C
from pycoap.client import Client
from pycoap.exceptions import NameResolutionError

SLOW_TESTS = False

class TestCoapMe(unittest.TestCase):

    def setUp(self):
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        
    def tearDown(self):
        self.loop.stop()
        self.loop.close()


    def test_coap_me_test(self):
        uri = "coap://coap.me/test"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertTrue( str(result.payload).startswith("welcome to the ETSI plugtest!") )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")

    #This is a long running test
    @unittest.skipUnless('--slow' in sys.argv, "Not runnig slow tests")
    def test_coap_me_seperate(self):
        uri = "coap://coap.me/separate"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertEqual( str(result.payload), "That took a long time")
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")

    #This is a long running test
    @unittest.skipUnless('--slow' in sys.argv, "Not runnig slow tests")
    def test_coap_me_large(self):
        uri = "coap://coap.me/large"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertTrue( str(result.payload).startswith("\n     0                   1                   2                   3") )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")
        self.assertEqual( len(result.payload) , 1700 )

    def test_coap_me_secret(self):
        uri = "coap://coap.me/secret"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertEqual( str(result.payload), "Not authorized" )
        self.assertEqual( str(result.code) , "4.01")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")


    def test_coap_me_weird33(self):
        uri = "coap://coap.me/weird33"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertEqual( str(result.payload), "resource with option 33" )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")
        self.assertIn( "Option-33", result.options)

    def test_coap_me_weird3333(self):
        uri = "coap://coap.me/weird3333"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertEqual( str(result.payload), "resource with option 3333" )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")
        self.assertIn( "Option-3333", result.options)

    def test_coap_me_create1(self):
        uri = "coap://coap.me/create1"
        payload = "da payload"
        client = Client()
        result = self.loop.run_until_complete( client.put(uri,payload) )   
        self.assertEqual( str(result.payload), "Created" )
        self.assertEqual( str(result.code) , "2.01")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")

    def test_coap_me_query_no_params(self):
        uri = "coap://coap.me/query"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertEqual( str(result.payload), "You asked me about: Nothing particular." )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")


    def test_coap_me_query_with_params(self):
        uri = "coap://coap.me/query?cats&dogs&frogs"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) )   
        self.assertEqual( str(result.payload), "You asked me about: cats&dogs&frogs" )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")

    def test_coap_me_multi_format_text(self):
        uri = "coap://coap.me/multi-format"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri, acceptFormat=C.FORMAT_TEXT) )   
        self.assertEqual( str(result.payload), "TD_COAP_CORE_20" )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")

    def test_coap_me_multi_format_xml(self):
        uri = "coap://coap.me/multi-format"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri, acceptFormat=C.FORMAT_XML) )   
        self.assertEqual( str(result.payload), "<TD_COAP_CORE_20/>" )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "application/xml")

    def test_coap_me_3(self):
        uri = "coap://coap.me/3"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) ) 
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "application/json")

    def test_coap_me_seg1_seg2_seg3(self):
        uri = "coap://coap.me/seg1/seg2/seg3"
        client = Client()
        result = self.loop.run_until_complete( client.get(uri) ) 
        self.assertEqual( str(result.payload), "Matroshka" )
        self.assertEqual( str(result.code) , "2.05")
        self.assertEqual( result.options["Content-Format"], "text/plain charset=utf-8")




if __name__ == '__main__':
    #logging.basicConfig( stream=sys.stderr, level=logging.DEBUG )
    unittest.main()
