#!/usr/bin/python3

import argparse
import os
import sys
import unittest
import logging

def main():
    """
    Test runner

    Note: method for skipping slow tests pulled from https://sateeshkumarb.wordpress.com/2011/06/16/skipping-tests-in-unittests/
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('test_name', nargs='?', default="test", help="The test name to run. (i.e. 'test.test_observe') Uses the same naming format as 'python3 -m unitest'.")
    parser.add_argument("-f", "--failfast", help="Stop runnning tests on the first failure", action="store_true")
    parser.add_argument("--slow", help='Run slow tests', action="store_true")
    arg_group = parser.add_argument_group()
    arg_group.add_argument("-v", help='Show test names', action="store_true")
    arg_group.add_argument("-vv", help='Show test names and  debug logs', action="store_true")
    
    args = parser.parse_args()

    #Enable Logging
    if args.vv:
        logging.basicConfig(level=logging.DEBUG)

    # Set the verbosity
    verbosity = 2 if args.v or args.vv else 1

    # Slow tests
    if args.slow:
        global RUN_SLOW_TESTS
        RUN_SLOW_TESTS = True

    # Create the test suite
    if args.test_name == "test":
        module_path = os.path.abspath( os.path.dirname(sys.argv[0]) )
        suite = unittest.TestLoader().discover(module_path, pattern = "test_*.py")
    else:
        suite = unittest.TestLoader().loadTestsFromName(args.test_name)

    #run the tests
    unittest.TextTestRunner(verbosity=verbosity, failfast=args.failfast).run(suite)


if __name__ == "__main__":
    main()
